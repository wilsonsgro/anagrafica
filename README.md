Anagrafica
 -----------

### Requirements

![Docker >= 17.04 ](https://badgen.net/badge/Docker/>=17.04/409be6?icon=docker)

![docker-compose >=1.8.0 ](https://badgen.net/badge/docker-compose/>=1.8/409be6?icon=docker)
 
### Setup

```bash
cp .env.dist .env
```

### Build

```bash
docker-compose up -d
```

### Composer install

```bash
docker-compose exec php composer install
```
if have problem with your memory RAM use this
```bash
docker-compose exec php php -d memory_limit=1 composer.phar install
```

### Composer require

```bash
docker-compose exec php composer require <vendor>
```

if have problem with your memory RAM use this
```bash
docker-compose exec php php -d memory_limit=1 composer.phar require <vendor>
```

### Commands to enter containers

```bash
docker-compose exec php bash
docker-compose exec mariadb bash
```

###  Migrate Phinx
```bash
docker-compose exec php php composer.phar require robmorgan/phinx
docker-compose exec php php composer.phar install --no-dev
docker-compose exec php vendor/bin/phinx init .
$EDITOR phinx.yml
docker-compose exec php mkdir -p db/migrations db/seeds
docker-compose exec php vendor/bin/phinx create AnagraficaMigration
docker-compose exec php vendor/bin/phinx migrate -e development
```

#### Hey oh Let's Go

[local.anagrafica](http://local.anagrafica)

### Commands Cli

```bash
docker-compose exec php php command/application.php greet
```

### Tests Functional

```bash
docker-compose exec php  ./vendor/bin/phpunit ./tests/Functional/anagraficaTest.php
```
with docker-machine
```bash
docker-compose exec php  nano /etc/hosts 192.168.99.100 local.anagrafica
```

### Tests Ping

```bash
docker-compose exec php  ping local.anagrafica
```

###  After create new file php
```bash
docker-compose exec php php composer.phar  dump-autoload -o
```

## Teach
> [Phinx migrate](https://phinx.org/)

> [Getting started with PHP-DI](http://php-di.org/doc/getting-started.html)

> [FastRoute - Fast request router for PHP](https://github.com/nikic/FastRoute)

> [Silly CLI micro-framework based on Symfony Console](http://mnapoli.fr/silly/)

> [PHP Code Checker](https://phpcodechecker.com/)

> [password_verify issues](https://www.experts-exchange.com/questions/28966548/password-verify-issues.html)

> [Html beautifier](https://www.freeformatter.com/html-formatter.html#ad-output)

> [Javascript beautifier](https://beautifier.io/)

> [regex101 ](https://regex101.com/)

> [Tutorial php regex ](https://www.tutorialrepublic.com/php-tutorial/php-regular-expressions.php)
