FROM composer:1.5.1 AS composer

FROM php:7.0.2-fpm

ARG mysql=1

RUN mkdir -p /usr/local/bin
COPY --from=composer /usr/bin/composer /usr/local/bin/composer
RUN pecl install \
        apcu && \
    docker-php-ext-enable apcu

RUN apt-get update && apt-get install  -y --no-install-recommends apt-utils && apt-get install -y \
        libicu-dev \
    && docker-php-ext-install \
        intl \
        opcache \
        bcmath \
        exif \
    && docker-php-ext-enable \
        intl \
        opcache \
        bcmath \
        exif

RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        libicu-dev \
        libicu52  \
        sudo  \
        acl  \
        nmap \
        nano \
        vim \
        openssh-client \
        openssh-server \
    && docker-php-ext-install iconv \
    && docker-php-ext-install exif \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install intl \
    && docker-php-ext-install opcache \
    && docker-php-ext-install zip \
    && docker-php-ext-install bcmath 

# Install db extensions
RUN if [ "$mysql" -eq "1" ]; then \
        docker-php-ext-install pdo pdo_mysql \
    ;fi

# Install gd extensions
RUN apt-get install -y \
            libfreetype6-dev \
            libjpeg62-turbo-dev \
            libpng-dev \
        && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
        && docker-php-ext-install gd

# Install useful tools for composer
RUN apt-get install -y git unzip

CMD ["php-fpm"]
