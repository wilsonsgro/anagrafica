<?php

require __DIR__ . '/../vendor/autoload.php';

use DI\ContainerBuilder;

session_start();

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions(__DIR__ . '/../config/settings.php');
$containerBuilder->addDefinitions(__DIR__ . '/../config/dependencies.php');
$container = $containerBuilder->build();

return $container;
