<?php

use FastRoute\RouteCollector;

$dispatcher = FastRoute\simpleDispatcher(function (RouteCollector $r) {
    $r->addRoute('GET', '/', ['Acme\Controller\AnagraficaController', 'homeAction'] );
    $r->addRoute('GET', '/list', ['Acme\Controller\AnagraficaController', 'listAction'] );
    $r->addRoute('GET', '/create', ['Acme\Controller\AnagraficaController', 'createAction'] );
    $r->addRoute('GET', '/delete', ['Acme\Controller\AnagraficaController', 'deleteAction'] );
    $r->addRoute('GET', '/update/{id}', ['Acme\Controller\AnagraficaController', 'updateAction'] );
});

$route = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

switch ($route[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo '404 Not Found';
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        echo '405 Method Not Allowed';
        break;

    case FastRoute\Dispatcher::FOUND:
        $controller = $route[1];
        $parameters = $route[2];

        // We could do $container->get($controller) but $container->call()
        // does that automatically
        $container->call($controller, $parameters);
        break;
}