<?php

use Acme\View\View;
use Acme\Model\Model;
use function DI\factory;
use Psr\Container\ContainerInterface;
use Acme\Controller\AnagraficaController;

return [
    'model' => function (ContainerInterface $c) {
        return new Model();
    },
    'view' => function (ContainerInterface $c) {
        return new View();
    },

    'Controller' => DI\factory(function (ContainerInterface $c) {
        return new AnagraficaController($c->get('model'), $c->get('view'));
    }),
];
