<?php

namespace Acme\Controller;

use Acme\View\View;
use Acme\Model\Model;

/**
 * Controller
 *
 */
class AnagraficaController
{
    private $model;
    private $view;

    /**
     * construct
     *
     */
	public function __construct(View $view, Model $model)
    {
        $this->view = $view;
        $this->model = $model;
    }

    public function homeAction()
    {
        $this->view->render("home.html");
    }

    public function listAction()
    {
        //$list = $this->model->list();
        $this->view->render("list.html");
    }

    public function createAction()
    {
        //  $createItem = $this->model->createItem();
        $this->view->render("create.html");
    }

    public function updateAction($id)
    {
        // $createItem = $this->model->updateItem($id);
        $this->view->render("update.html", $id);
    }

    public function deleteAction($id)
    {
        //  $list = $this->model->delete($id);
        $this->view->render("list.html");
    }
}
