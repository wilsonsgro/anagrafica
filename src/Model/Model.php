<?php

namespace Acme\Model;

use Acme\Util\Util;

/**
 * Model
 *
 */
class Model
{
    public function __construct()
    {

    }
    /**
     * function getConnect
     *
     */
    private function getConnect()
    {

    }

    /**
     * function getList
     *@return mixed
     */
    private function getList()
    {

    }

    /**
     * function createItem
     *@return mixed
     */
    private function createItem($value)
    {

    }

    /**
     * function updateItem
     *@return mixed
     */
    private function updateItem($value, $id)
    {

    }
}
