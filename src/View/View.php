<?php

namespace Acme\View;

/**
 * View
 *
 */
class View
{
    const TEMPLATE_DIR = 'src/View/Template/';
    private     $templateDir;

    public function __construct()
    {
        $this->templateDir = View::TEMPLATE_DIR;
    }

    /**
     * render
     *
     */
    public function render($templateFile)
    {
        if (file_exists($this->templateDir.$templateFile)) {
            include $this->templateDir.$templateFile;
        } else {
            throw new Exception('Template file ' . $templateFile . ' is not present in directory ' . $this->templateDir);
        }
    }
}
