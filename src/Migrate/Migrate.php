<?php

namespace Acme\Migrate;

use Acme\Util\Util;

/**
 * Migrate
 *
 */
class Migrate
{
    private $host;
    private $user;
    private $password;
    private $dbName;

    /**
     * __construct
     *
     */
    public function __construct()
    {
        $this->host = Util::HOST_DOCKER;
        $this->user = Util::USER;
        $this->password = Util::PASSOWORD;
        $this->dbName = Util::DB_NAME;
    }

    /**
     * function getConnect
     *
     */
    private function getConnect()
    {
        $con = mysqli_connect($this->host, $this->user, $this->password, $this->dbName);
        if (mysqli_connect_errno())  {
            die("Failed to connect to MySQL: " . mysqli_connect_error());
        }

        return $con;
    }

    /**
     * function exitstDatase
     *
     */
    public function exitstDatase()
    {
        $con = $this->getConnect();

        $sqlCheckTablefile =  Util::PATH_CHECK_TABLE;
        if (!file_exists($sqlCheckTablefile)) {
            die("The file $sqlCheckTablefile not exists");
        }

        $sqlScript = file_get_contents($sqlCheckTablefile);

        if (mysqli_query($con,$sqlScript))  {
            if (mysqli_query($con,$sqlScript))  {
                // exist create
                return true;
            }
            else  {
                return false;
            }
            mysqli_close($con);
        }
        else  {
            //!TODO
        }
        mysqli_close($con);
    }

    /**
     * function create
     *
     */
    public function create()
    {
        try {
                $con = $this->getConnect();

                $sqlCreateTablefile =  Util::PATH_CREATE_TABLE;
                if (!file_exists($sqlCreateTablefile)) {
                    die("The file $sqlCreateTablefile not exists");
                }

                $sqlScript = file_get_contents($sqlCreateTablefile);

                if (mysqli_query($con,$sqlScript))  {
                    // table create
                    return true;
                }
                else  {
                    return false;
                }
                mysqli_close($con);

            } catch (Exception $e) {
                die( 'Caught exception: ' .  $e->getMessage());
            }
    }

    /**
     * function drop
     *
     */
    public function drop()
    {
        try {
            $con = $this->getConnect();

            $sqlDropTablefile =  Util::PATH_DROP_TABLE;
            if (!file_exists($sqlDropTablefile)) {
                die("The file $sqlDropTablefile not exists");
            }

            $sqlScript = file_get_contents($sqlDropTablefile);

            if (mysqli_query($con,$sqlScript))  {
                // drop create
                return true;
            }
            else  {
                return false;
            }
            mysqli_close($con);
        } catch (Exception $e) {
            die( 'Caught exception: ' .  $e->getMessage());
        }
    }
}
